import axios from 'axios';
import { service } from '.';



const baseURL = process.env.REACT_APP_API_URL;
export default {
  login: (data) => service.post('/auth/v1/web/sign-in', data),
  forgotPass: (data) => service.post('/auth/v1/web/forgot-password', data),

  // getToken: (data) =>
  //   axios.post(
  //     'https://keycloak.carbozor.uz/auth/realms/carbozor/protocol/openid-connect/token',
  //     data,
  //     {
  //       headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  //     }
  //   ),
  // logOutuser: (data) =>
  //   axios.post(
  //     'https://keycloak.carbozor.uz/auth/realms/carbozor/protocol/openid-connect/logout',
  //     data,
  //     {
  //       headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  //     }
  //   ),

};
