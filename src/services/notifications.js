import { service } from '.';

export default {
  getAll: () => service.post('/notification/v1/notification/list-for-web'),
  getOne: (id) => service.get(`notification/v1/notification/one-for-web//${id}`),
  create: (data) => service.get('/notification/v1/notification/send-test', data),
  // update: (id, data, lng) => service.patch(`/${id}`, data),
  imageUpload: (data) =>
    service.get('notification/v1/attachment/upload-photo', data, {
      headers: {
        'Accept-Language': 'en-US,en;q=0.8',
        'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
      }
    }),
  imageDownload: (data) =>
    service.get('notification/v1/attachment/upload-photo'),
  // deleteImage: (data) => service.delete('', { data }),
};
