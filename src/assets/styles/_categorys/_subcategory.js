import { createStyles, makeStyles, styled } from '@mui/styles';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'flex',
      minWidth: '100%',
      flexGrow: 1,
      padding: 10,
      justifyContent: 'center',
      alignItems: 'center',
      // marginTop: 300, // need check hd screen
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    formControl: { width: '100%' },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    saveButton: {
      margin: theme.spacing(1),
      padding: theme.spacing(1, 5),
      width: '15%',
    },
    cancelButton: {
      margin: theme.spacing(1),
      padding: theme.spacing(1, 5),
      width: '15%',
    },
    switch: {
      alignSelf: 'center',
    },
    control: {
      padding: theme.spacing(1),
      display: 'flex',
      justifyContent: 'start',
      alignItems: 'center',
    },
    footerBtn: {
      padding: theme.spacing(1),
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'end',
    },
  })
);
