import { createStyles, makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme) => createStyles({
  root: {
    minWidth: '100%',
    flexGrow: 1,
    padding: 10
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  formControl: { width: '100%' },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  button: {
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    width: '100%'
  },
  controlBtns: {
    padding: theme.spacing(1),
    display: 'flex',
    justifyContent: 'space-around'
  }
}));
