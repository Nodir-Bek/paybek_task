import { createStyles, makeStyles } from '@mui/styles';
import React from 'react';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
      margin: '1% auto',
      padding: theme.spacing(3),
    },
    paper: {
      width: '100%',
      marginTop: theme.spacing(3),
      // display: 'flex',
      // alignItems: 'center'
    },
    previewChip: {
      minWidth: 160,
      maxWidth: 210,
    },
    margin: {
      margin: theme.spacing(0),
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    errorText: {
      color: 'red',
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    formControl: {
      margin: theme.spacing(1),
      width: '100%',
    },
    input: {
      '& input[type=number]': {
        '-moz-appearance': 'textfield',
      },
      '& input[type=number]::-webkit-outer-spin-button': {
        '-webkit-appearance': 'none',
        margin: 0,
      },
      '& input[type=number]::-webkit-inner-spin-button': {
        '-webkit-appearance': 'none',
        margin: 0,
      },
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
    addNewBtn: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'start',
      gap: 5,
      marginTop: '20px',
      width: '100%',
      border: '1px solid rgba(47, 46, 46, 0.2)',
      borderRadius: '8px',
      padding: 14,
      background: 'transparent',
      cursor: 'pointer',
    },
    btnGroup: {
      width: '100%',
      marginTop: '20px',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'end',
      alignItems: 'center',
      gap: '20px',
    },
    switch: {
      marginTop: theme.spacing(1),
      display: 'flex',
      alignSelf: 'center',
      justifyContent: 'end',
      [theme.breakpoints.down('md')]: {
        marginTop: theme.spacing(1),
        justifyContent: 'flex-start',
        alignSelf: 'start',
      },
    },
    breadCrumb: {
      display: 'flex',
      alignSelf: 'center',
      justifyContent: 'start',
    },
    acord: {
      my: 5,
    },
  })
);
