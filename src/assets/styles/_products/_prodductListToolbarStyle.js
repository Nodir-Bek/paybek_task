import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(2),
    padding: theme.spacing(1)
  },
  controlBtns: {
    margin: theme.spacing(1)
  },
  dropDownMenu: {
    display: 'flex',
    justifyContent: 'space-between'
  }
}));
