/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import { Navigate } from 'react-router-dom';
import DashboardLayout from '../components/Layout/DashboardLayout';
import MainLayout from '../components/Layout/MainLayout';
import Dashboard from '../views/Dashboard';
import Login from '../views/Auth/Login';
import NotFound from '../views/404';
import ForgotPass from '../views/Auth/ForgotPass';
import Notifications from '../components/Notifications/List';
import AddNotifications from '../components/Notifications/Add';

export const protectedRoutes = [
  {
    element: <DashboardLayout />,
    children: [
      { index: true, path: '/app/Notification', element: <Dashboard /> },
      // { path: '/app/notification', element: <Notifications /> },
      // { path: '/app/notification/create', element: <AddNotifications /> },
      { path: '*', element: <Navigate to="/app/Notification" /> },
    ],
  },
];
export const publicRoutes = [
  {
    element: <MainLayout />,
    children: [
      { path: '/', element: <Login /> },
      { path: '/login', element: <Login /> },
      { path: '/forgot-password', element: <ForgotPass /> },
      { path: '404', element: <NotFound /> },
      { path: '*', element: <Navigate to="/login" /> },
    ],
  },
];
