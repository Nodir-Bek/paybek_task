import { useState } from 'react';
import { Helmet } from 'react-helmet';
import { Box, Button, Container, Grid, Tabs, Tab } from '@mui/material';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import LabTabs from '../../components/Tabs';

const Dashboard = () => {
  const [value, setValue] = useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Helmet>
        <title> PayBek | Dashboard</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 12,
          px: 6,
        }}
      >
        <Box sx={{ width: '100%', typography: 'body1' }}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            {/* <Tabs onChange={handleChange} aria-label="lab API tabs example">
              <Tab
                label="Page1"
                to="/app/notification/create"
                component={Link}
              />
              <Tab label="Page2" to="/app/notification" component={Link} />
            </Tabs> */}
            <LabTabs />
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default Dashboard;
