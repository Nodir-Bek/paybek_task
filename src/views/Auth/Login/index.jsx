import { useNavigate } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import {
  Box,
  Button,
  Card,
  Container,
  TextField,
  Typography,
  Stack,
  Alert,
  Grid,
} from '@mui/material';
import { useDispatch } from 'react-redux';
import auth from '../../../services/auth';
import { setToken } from '../../../redux/modules/auth/actions';
import { useStyles } from './style';

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const schema = Yup.object().shape({
    password: Yup.string().max(255).required('Password is required'),
    email: Yup.string().email('Invalid email').required('Required'),
  });

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: schema,
    onSubmit: (values) => {
      // const data = new URLSearchParams();
      // data.append('email', values.email);
      // data.append('password', values.password);
      // data.append('grant_type', 'password');
      // data.append('client_id', 'carbozor');
      // data.append('client_secret', 'GUfoXq8PQ6ki33RNTHFMArpALNILX0jM');
      const data = {
        email: values.email,
        password: values.password,
      };

      auth
        .login(data)
        .then((resp) => {
          console.log('resp auh', resp);
          if (resp.success) {
            console.log('resp succes', resp.data);
            localStorage.setItem('token', resp.data.accessToken);
            localStorage.setItem('refresh_token', resp.data.refreshToken);
            dispatch(setToken(resp.data.accessToken));
          }
        })
        .catch((err) => {
          toast.error('Error, password or login entered incorrectly!');
          console.log('resp eror', err);
        });
    },
  });

  const handleForgot = () => {
    console.log('forgot clicked');
  };
  return (
    <>
      <Helmet>
        <title>Login | PayBek Admin</title>
      </Helmet>
      <Box className={classes.root}>
        <Container
          maxWidth="sm"
          sx={{
            p: 5,
            borderRadius: '8px',
          }}
        >
          <form
            className={classes.form}
            onSubmit={formik.handleSubmit}
            autoComplete="off"
          >
            <Box>
              <div className={classes.formFileds}>
                <Typography sx={{ width: '50%' }} variant="h5">
                  Email
                </Typography>
                <TextField
                  error={Boolean(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  margin="normal"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="test"
                  value={formik.values.email}
                  variant="outlined"
                  size="small"
                  autoComplete="off"
                  sx={{ borderRadius: '8px' }}
                />
              </div>
              <div className={classes.formFileds}>
                <Typography sx={{ width: '50%' }} variant="h5">
                  Password
                </Typography>
                <TextField
                  error={Boolean(
                    formik.touched.password && formik.errors.password
                  )}
                  fullWidth
                  helperText={formik.touched.password && formik.errors.password}
                  margin="normal"
                  name="password"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.password}
                  variant="outlined"
                  autoComplete="off"
                  size="small"
                />
              </div>
            </Box>
            <Box className={classes.submitSection}>
              <Button
                // disabled={formik.isSubmitting}
                size="large"
                type="submit"
                variant="contained"
                sx={{
                  my: 6,
                  padding: '8px, 14px',
                  borderRadius: '8px',
                  backgroundColor: '#0085FF',
                  textTransform: 'capitalize',
                  '&:hover': {
                    backgroundColor: '#0085FF60',
                  },
                }}
              >
                Login
              </Button>
              <Button
                onClick={handleForgot}
                sx={{
                  color: '#969696',
                  textTransform: 'none',
                }}
              >
                Forgot your password?
              </Button>
            </Box>
          </form>
          <Box>
            <Stack sx={{ width: '100%' }} spacing={2}>
              <Alert severity="info">
                <Typography>email: email@gmail.com</Typography>
                <Typography>password: Root_123</Typography>
              </Alert>
            </Stack>
          </Box>
        </Container>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </Box>
    </>
  );
};

export default Login;
