import { FlashlightOnTwoTone } from '@mui/icons-material';

export const notificationsHeader = [
  {
    id: 1,
    Header: 'ID',
    accessor: 'id',
    show: true,
  },
  {
    id: 2,
    Header: 'Country',
    accessor: 'product_name',
    show: true,
  },
  {
    id: 3,
    Header: 'Language',
    accessor: 'category_name',
    show: true,
  },
  {
    id: 4,
    Header: 'Title',
    accessor: 'subcategory_name',
    show: true,
  },
  {
    id: 5,
    Header: 'Sent date',
    accessor: 'total_product_type',
    show: true,
  },
  {
    id: 6,
    Header: 'Deliveries',
    accessor: 'is_have_promotion',
    show: true,
  },
  {
    id: 7,
    Header: 'Opens',
    accessor: 'total_quantity',
    show: true,
  },
];
