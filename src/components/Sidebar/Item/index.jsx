import React, { useState } from 'react';
import {
  NavLink as RouterLink,
  matchPath,
  useLocation,
  NavLink,
} from 'react-router-dom';
import { useTheme } from '@mui/styles';
import PropTypes from 'prop-types';
import { Button, ListItem, Collapse, ListItemIcon } from '@mui/material';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { clearToken } from '../../../redux/modules/auth/actions';
import auth from '../../../services/auth';

import { useStyles } from './style';

const NavItem = ({ href, icon: Icon, title, logout, subitems, ...rest }) => {
  const classes = useStyles();

  const [isOpen, setIsOpen] = useState(false);
  const [navActive, setNavActive] = useState(false);
  const { t } = useTranslation();
  const theme = useTheme();

  const location = useLocation();
  const dispatch = useDispatch();
  const handleLogOut = () => {
    const data = new URLSearchParams();
    data.append('client_id', 'carbozor');
    data.append('client_secret', 'GUfoXq8PQ6ki33RNTHFMArpALNILX0jM');
    data.append('refresh_token', localStorage.getItem('refresh_token'));
    localStorage.removeItem('token');

    auth
      .logOutuser(data)
      .then((res) => dispatch(clearToken()))
      .catch((err) => console.log(err));
  };
  const handleColapse = () => {
    setIsOpen(!isOpen);
  };

  const active = location.pathname.startsWith(href);
  const currentOpen = location.pathname;

  // const checkPathName = (href) => {
  //   let resp;
  //   if (href === currentOpen) {
  //     resp = true;
  //   }
  //   return resp;
  // };

  const handleActive = (url) => {
    // const pathName = location.pathname;
    // if (pathName === url) {
    //   setIsactive(true);
    // } else {
    //   setIsactive(false);
    // }
  };

  // if (subitems) {
  //   return (
  //     <>
  //       <ListItem
  //         disableGutters
  //         sx={{
  //           display: 'flex',
  //           py: 0,
  //         }}
  //         {...rest}
  //       >
  //         <Button
  //           component={NavLink}
  //           onClick={handleColapse}
  //           // className={(currentNav) =>
  //           //   currentNav.isActive ? classes.navActive : ''
  //           // }
  //           sx={{
  //             color: '#fff',
  //             // color: 'text.secondary',
  //             fontWeight: 'medium',
  //             justifyContent: 'space-between',
  //             alignItems: 'center',
  //             letterSpacing: 0,
  //             py: 1.25,
  //             textTransform: 'none',
  //             width: '100%',
  //             ...(active && {
  //               color: 'primary.active',
  //               '&:after': {
  //                 content: '" "',
  //                 position: 'absolute',
  //                 top: 0,
  //                 left: '-15px',
  //                 height: '100%',
  //                 width: '3px',
  //                 borderTopRightRadius: 3,
  //                 borderBottomRightRadius: 3,
  //                 backgroundColor: 'primary.active',
  //               },
  //             }),
  //             '& svg': {
  //               mr: 1,
  //             },
  //           }}
  //           to={href}
  //         >
  //           <div
  //             style={{
  //               display: 'flex',
  //               justifyContent: 'space-between',
  //               alignItems: 'center',
  //             }}
  //           >
  //             {Icon && <Icon size="20" active={active ? 'true' : ''} />} {title}
  //           </div>
  //           <ListItemIcon
  //             sx={{
  //               pl: 5,
  //             }}
  //           >
  //             {isOpen ? <ToDownIcon /> : <ToRightIcon color={active} />}
  //           </ListItemIcon>
  //         </Button>
  //       </ListItem>
  //       {subitems && (
  //         <Collapse
  //           in={isOpen}
  //           timeout="auto"
  //           unmountOnExit
  //           sx={{
  //             paddingLeft: theme.spacing(10) + 30,
  //             width: '100%',
  //             position: 'relative',
  //             '&:after': {
  //               content: '" "',
  //               position: 'absolute',
  //               top: 10,
  //               bottom: 0,
  //               left: '5%',
  //               height: '88%',
  //               width: '0.5px',
  //               opacity: 0.2,
  //               bgcolor: '#fff',
  //             },
  //           }}
  //         >
  //           <ListItem
  //             component="div"
  //             disablePadding
  //             disableGutters
  //             sx={{
  //               display: 'flex',
  //               flexDirection: 'column',
  //               width: '100%',
  //               py: 0,
  //               pl: 4,
  //             }}
  //             {...rest}
  //           >
  //             {subitems.map((childrenLink) => (
  //               <Button
  //                 key={childrenLink.href}
  //                 component={NavLink}
  //                 onClick={() => handleActive(childrenLink.href)}
  //                 sx={{
  //                   color: '#fff',
  //                   // color: 'text.secondary',
  //                   fontWeight: 'medium',
  //                   justifyContent: 'flex-start',
  //                   letterSpacing: 0,
  //                   py: 1.25,
  //                   textTransform: 'none',
  //                   width: '100%',
  //                   ...(active && {
  //                     borderRadius: '4px',
  //                     backgroundColor: '#4C4B4B',
  //                     color: '#fff',
  //                   }),
  //                   '& svg': {
  //                     mr: 1,
  //                   },
  //                 }}
  //                 to={childrenLink.href}
  //               >
  //                 {childrenLink.Icon && <Icon size="20" />}
  //                 <span>{childrenLink.title}</span>
  //               </Button>
  //             ))}
  //           </ListItem>
  //         </Collapse>
  //       )}
  //     </>
  //   );
  // }
  return (
    <ListItem
      disableGutters
      sx={{
        display: 'flex',
        py: 0,
      }}
      {...rest}
    >
      <Button
        component={RouterLink}
        onClick={logout ? handleLogOut : null}
        sx={{
          color: '#1C1C1C',
          // color: 'text.secondary',
          fontWeight: 'medium',
          justifyContent: 'flex-start',
          letterSpacing: 0,
          py: 1.25,
          textTransform: 'none',
          width: '100%',
          ...(active && {
            backgroundColor: '#E5F3FF',
          }),
          '& svg': {
            mr: 1,
          },
        }}
        to={href}
      >
        {Icon && <Icon size="20" active={active} />}
        <span>{title}</span>
      </Button>
    </ListItem>
  );
};

NavItem.propTypes = {
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string,
  subitems: PropTypes.array,
};

export default NavItem;
