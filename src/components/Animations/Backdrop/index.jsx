import { useState } from 'react';
import { GridLoader } from 'react-spinners';
import Backdrop from '@mui/material/Backdrop';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import './style.css';

const override = `
  display: block;
  margin: 0 auto;
  border-color: red;
`;

export default ({ loading }) => {
  return (
    <div className="sweet-loading">
      <Backdrop
        sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
        className="container"
      >
        <Typography variant="h4" color="white">
          LOADING...
        </Typography>
        <GridLoader color="#fff" loading={loading} css={override} size={30} />
      </Backdrop>
    </div>
  );
};
