import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
} from '@mui/material';
import { green, indigo } from '@mui/material/colors';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { useTranslation } from 'react-i18next';
import { deliveredData } from '../helper';

export default ({ ...rest }) => {
  const { t } = useTranslation();
  return (
    <Card {...rest}>
      <CardContent>
        <Grid container spacing={3} sx={{ justifyContent: 'space-between' }}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="h6">
              {t(`${deliveredData.title}`)}
            </Typography>
            <Typography color="textPrimary" variant="h3">
              {deliveredData.count}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: indigo[600],
                height: 56,
                width: 56,
              }}
            >
              <LocalShippingIcon />
            </Avatar>
          </Grid>
        </Grid>
        <Box
          sx={{
            alignItems: 'center',
            display: 'flex',
            pt: 2,
          }}
        >
          <ArrowUpwardIcon sx={{ color: green[900] }} />
          <Typography
            variant="body2"
            sx={{
              color: green[900],
              mr: 1,
            }}
          >
            {deliveredData.percent}
          </Typography>
          <Typography color="textSecondary" variant="caption">
            {t('Since last month')}
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};
