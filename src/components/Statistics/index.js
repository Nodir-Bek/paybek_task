export { default as Delivered } from './Delivery';
export { default as Income } from './Income';
export { default as LatestOrder } from './Income';
export { default as OrdersStatistics } from './OrderStatistics';
export { default as SalesRegions } from './Regions';
export { default as Visits } from './Visits';
export { default as WaitingStatistics } from './Waiting';
