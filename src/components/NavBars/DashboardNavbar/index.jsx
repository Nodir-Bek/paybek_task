import { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  AppBar,
  Badge,
  Box,
  Hidden,
  IconButton,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
  makeStyles,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/NotificationsOutlined';
// import InputIcon from '@mui/icons-material/Input';
import Logo from '../../Logo';
import SelectLanguageBtn from '../../Language';
import { Navs } from './helper';

const Navbar = ({ onMobileNavOpen, ...rest }) => {
  const [notifications] = useState([]);
  const [anchorElNav, setAnchorElNav] = useState(null);

  return (
    <AppBar
      elevation={2}
      {...rest}
      sx={{
        width: '100%',
        background: '#fff',
        // backgroundColor: 'background.default',
        boxShadow: '0px 2px 10px rgba(0, 0, 0, 0.1)',
      }}
    >
      <Toolbar
        sx={{
          height: 120,
          backgroundColor: '#fff',
          width: '100%',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          zIndex: 20,
        }}
      >
        <RouterLink to="/">
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              // gap: 4,
            }}
          >
            <Box
              sx={{
                padding: 1,
                borderRadius: '8px',
                border: '1px solid #969696',
              }}
            >
              <Logo Width="70" Height="70" />
            </Box>
            <Box>
              <Typography
                sx={{
                  color: '#1C1C1C',
                  fontWeight: 'bold',
                }}
              >
                Paybek Inc.
              </Typography>
              <Typography
                sx={{
                  color: '#969696',
                  fontWeight: 'normal',
                }}
              >
                New York, NY
              </Typography>
            </Box>
          </Box>
        </RouterLink>
        <Box sx={{ flexGrow: 1 }} />
        <Hidden>
          <Box
            sx={{
              ml: 20,
              width: '100%',
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              gap: 6,
            }}
          >
            {Navs.map((item, indx) => (
              <RouterLink key={`${indx + 1}`} to={item.href}>
                <Typography
                  sx={{
                    color: item.active ? '#0085FF' : '#585757',
                    fontWeight: 'bold',
                  }}
                >
                  {item.title}
                </Typography>
              </RouterLink>
            ))}
          </Box>
          <IconButton
            sx={{
              color: 'darker.default',
            }}
            size="large"
          >
            <Badge
              badgeContent={notifications.length}
              color="primary"
              variant="dot"
            >
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <SelectLanguageBtn />
        </Hidden>
        <Hidden lgUp>
          <IconButton onClick={onMobileNavOpen} size="large">
            <MenuIcon
              sx={{
                color: 'darker.default',
              }}
            />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Navbar.propTypes = {
  onMobileNavOpen: PropTypes.func,
};

export default Navbar;
