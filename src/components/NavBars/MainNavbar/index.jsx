import { Link as RouterLink } from 'react-router-dom';
import { AppBar, Toolbar, Box, Button, Typography } from '@mui/material';
import Logo from '../../Logo';

const MainNavbar = ({ ...rest }) => (
  <AppBar elevation={2} {...rest}>
    <Toolbar
      sx={{
        height: 124,
        backgroundColor: '#fff',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <RouterLink to="/">
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            gap: 4,
          }}
        >
          <Box
            sx={{
              padding: 1,
              borderRadius: '8px',
              border: '1px solid #969696',
            }}
          >
            <Logo Width="70" Height="70" />
          </Box>
          <Box>
            <Typography
              sx={{
                color: '#1C1C1C',
                fontWeight: 'bold',
              }}
            >
              Paybek Inc.
            </Typography>
            <Typography
              sx={{
                color: '#969696',
                fontWeight: 'normal',
              }}
            >
              New York, NY
            </Typography>
          </Box>
        </Box>
      </RouterLink>
      <Button
        sx={{
          color: '#969696',
          fontWeight: 'bold',
          textTransform: 'Capitalize',
        }}
      >
        admin panel
      </Button>
    </Toolbar>
  </AppBar>
);

export default MainNavbar;
