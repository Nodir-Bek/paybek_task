import React, { useState } from 'react';
import { Box } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { byTill, byStatus, Form, byCountry, byLanguage } from './helper';
import { useStyles } from '../style';
import FilterButton from '../../FormElements/FilterButton';

export default () => {
  const classes = useStyles();
  const [status, setStatus] = useState('');
  const [category, setCategory] = useState('');
  const [subCategory, setSubCategory] = useState('');
  const [promotion, setPromotion] = useState('');
  const [selectedFilter, setSelectedFilter] = useState(0);

  const handleStatus = (e) => {
    setStatus(e.target.value ? '1' : '0');
    if (!status) {
      setSelectedFilter((prev) => prev + 1);
    }
  };
  const handlePromotion = (event) => {
    setPromotion(event.target.value);
    if (!promotion) {
      setSelectedFilter((prev) => prev + 1);
    }
  };

  const handleCategory = (event) => {
    setCategory(event.target.value);
    if (!category) {
      setSelectedFilter((prev) => prev + 1);
    }
  };

  const handleSubcategory = (event) => {
    setSubCategory(event.target.value);
    if (!subCategory) {
      setSelectedFilter((prev) => prev + 1);
    }
  };
  const handleClearFilters = () => {
    setStatus('');
    setSubCategory('');
    setCategory('');
    setSelectedFilter(0);
  };

  return (
    <Box className={classes.wrapper}>
      <Box className={classes.root}>
        <FormControl size="small">
          <InputLabel id="subcategory"> Till</InputLabel>
          <Select
            className={classes.input}
            value={subCategory}
            onChange={handleSubcategory}
            labelId="till"
            size="small"
            label="Till"
          >
            {byLanguage.map((item, index) => (
              <MenuItem key={`${index + 1}`} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl size="small">
          <InputLabel id="subcategory"> Till</InputLabel>
          <Select
            className={classes.input}
            value={subCategory}
            onChange={handleSubcategory}
            labelId="till"
            size="small"
            label="Till"
          >
            {byCountry.map((item, index) => (
              <MenuItem key={`${index + 1}`} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl size="small">
          <InputLabel id="form"> Form</InputLabel>
          <Select
            className={classes.input}
            value={category}
            onChange={handleCategory}
            labelId="form"
            size="small"
            label="Form"
          >
            {byTill.map((item, index) => (
              <MenuItem key={`${index + 1}`} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl size="small">
          <InputLabel id="subcategory"> Till</InputLabel>
          <Select
            className={classes.input}
            value={subCategory}
            onChange={handleSubcategory}
            labelId="till"
            size="small"
            label="Till"
          >
            {Form.map((item, index) => (
              <MenuItem key={`${index + 1}`} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl size="small">
          <InputLabel id="status"> Filter by status</InputLabel>
          <Select
            className={classes.input}
            value={status}
            onChange={handleStatus}
            labelId="status"
            size="small"
            label="Filter by status"
          >
            {byStatus.map((item, index) => (
              <MenuItem key={`${index + 1}`} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
    </Box>
  );
};
