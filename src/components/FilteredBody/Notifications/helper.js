export const byTill = [
  {
    name: 'Car accessories',
    value: 1,
  },
  {
    name: 'Car tire',
    value: 2,
  },
  {
    name: 'Car window',
    value: 3,
  },
  {
    name: 'Bathroom equipment',
    value: 4,
  },
  {
    name: 'Kitchen appliances',
    value: 5,
  },
];
export const Form = [
  {
    name: 'Form1',
    value: 1,
  },
  {
    name: 'Form2',
    value: 2,
  },
  {
    name: 'Form3',
    value: 3,
  },
];
export const byCountry = [
  {
    name: 'Uzbekistan',
    value: 1,
  },
  {
    name: 'Japan',
    value: 2,
  },
];
export const byLanguage = [
  {
    name: 'uz',
    value: 1,
  },
  {
    name: 'ru',
    value: 2,
  },
  {
    name: 'en',
    value: 3,
  },
];
export const byStatus = [
  {
    name: 'ACTIVE',
    value: 1,
  },
  {
    name: 'NOACTIVE',
    value: 0,
  },
];
