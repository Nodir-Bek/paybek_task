import React from 'react';
import { GoogleMap, withGoogleMap, withScriptjs } from 'react-google-maps';

function CustomMap() {
  return (
    <GoogleMap
      defaultZoom={10}
      defaultCenter={{ lat: 41.377491, lng: 64.585258 }}
    />
  );
}

const wrappedMap = withScriptjs(withGoogleMap(CustomMap));

export default wrappedMap;
