import { React, useMemo, useState } from 'react';
import { Helmet } from 'react-helmet';
import {
  Box,
  Button,
  Card,
  Container,
  InputAdornment,
  SvgIcon,
  TextField,
  Typography,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { AddCircleOutline as Add } from '@mui/icons-material';
import { AnimatePresence } from 'framer-motion';
import {
  Sliders as FilterIcon,
  Search as SearchIcon,
  ArrowDown as ExportIcon,
  ChevronDown as DownIcon,
} from 'react-feather';
import FilterTools from '../../FilteredBody/Notifications';
import { toolTips } from './helper';
import { useStyles } from './style';
import Table from '../../Table';
import { notificationsHeader } from '../../../redux/modules/table/common';
import { headerMaker } from '../../Table/helper';
import { fetchData } from '../../../redux/modules/notifications/actions';
import { useQuery } from '../../../hooks';

export default ({ ...rest }) => {
  const classes = useStyles();
  const [isShow, setIsShow] = useState(false);
  const { search, status, setSearch, setStatus, handleOnTableChange } =
    useQuery({ fetchData });

  const navigate = useNavigate();
  const { data, total, loading } = useSelector(
    (state) => state.notificationsReducer
  );
  console.log('data', data);
  const data1 = [];
  const headers = useMemo(
    () => headerMaker(notificationsHeader),
    [notificationsHeader]
  );
  const addCategory = () => {
    navigate('/app/manage/create');
  };

  return (
    <>
      <Helmet>
        <title>Notifications | Admin </title>
      </Helmet>
      <Box
        sx={{
          position: 'relative',
          backgroundColor: 'background.default',
          minHeight: '100%',
        }}
      >
        <Container
          maxWidth={false}
          sx={{
            py: 3,
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'row-reverse',
              alignItems: 'flex-end',
              gap: 2,
              width: '100%',
            }}
          >
            <div className={classes.navFilterSection}>
              <Box sx={{ mt: 2 }}>
                <TextField
                  className={classes.search}
                  size="small"
                  fullWidth
                  sx={{
                    height: 40,
                    maxWidth: '90%',
                    background: '#FFFFFF',
                    border: '1px solid rgba(47, 46, 46, 0.2)',
                    borderRadius: '6px',
                  }}
                  value={search}
                  onChange={(e) => setSearch(e.target.value)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SvgIcon fontSize="small" color="action">
                          <SearchIcon />
                        </SvgIcon>
                      </InputAdornment>
                    ),
                  }}
                  placeholder="Search product"
                  variant="outlined"
                />
              </Box>
            </div>
          </div>
          <div className={classes.topBtns}>
            <FilterTools />
            <Button
              onClick={() => setIsShow(!isShow)}
              sx={{
                backgroundColor: '#0085FF',
                color: '#fff',

                border: '1px solid rgba(47, 46, 46, 0.2)',
                // width: 111,
                // height: 40,
                borderRadius: '8px',
                px: 6,
                py: 1,
              }}
            >
              Search
            </Button>
          </div>
          <Box sx={{ pt: 5 }}>
            <div className={classes.root}>
              <Card {...rest}>
                <Table
                  data={data}
                  headers={headers}
                  toolTips={toolTips}
                  total={total}
                  onChange={handleOnTableChange}
                />
              </Card>
            </div>
          </Box>
        </Container>
      </Box>
    </>
  );
};
