import * as Yup from 'yup';

export const ValidateSchema = Yup.object().shape({
    userStatus: Yup.string()
        .required('Required'),
    countryId: Yup.string()
        .required('Required'),
    language: Yup.string()
        .required('Required'),
    title: Yup.string()
        .required('Required'),
    text: Yup.string()
        .required('Required'),
    testPhoneNumber: Yup.string()
        .required('Required'),
});

export const initialSchema = {
    id: '8125fdcc-bf1e-4fcc-b35b-a75e119d86d1',
    userStatus: '',
    countryId: '',
    language: '',
    title: '',
    text: '',
    photoId: '',
    sentDate: '',
    testPhoneNumber: ''
};

export const userType = [
    { id: 0, name: 'Active', value: 'ACTIVE' },
    { id: 1, name: 'Not active', value: 'NOT_ACTIVE' }
];

export const Countries = [
    {
        id: Date.now(),
        name: 'Uzbekistan',
        value: Date.now().toString(),
    },
    {
        id: Date.now(),
        name: 'Japan',
        value: Date.now().toString(),
    },
    {
        id: Date.now(),
        name: 'Italy',
        value: Date.now().toString(),
    },
    {
        id: Date.now(),
        name: 'France',
        value: Date.now().toString(),
    }
];

export const Langs = [
    {
        id: 0,
        name: 'English',
        value: 'en',
    },
    {
        id: 1,
        name: 'Russian',
        value: 'ru',
    },
    {
        id: 2,
        name: 'Uzbek',
        value: 'uz',
    }
];
