/* eslint-disable import/named */
/* eslint-disable import/no-named-as-default */
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Box,
  Button,
  Card,
  colors,
  Grid,
  Input,
  Stack,
  TextField,
  Typography,
  FormControl,
  Radio,
} from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import SaveBtn from '../../FormElements/Buttons/SaveButton';
import SelectInput from '../../FormElements/Select';
import BasicDatePicker from '../../FormElements/DatePicker';
import {
  initialSchema,
  ValidateSchema,
  Countries,
  Langs,
  userType,
} from './helper';
import notifications from '../../../services/notifications';

const CreateForm = () => {
  const navigate = useNavigate();
  const { state } = useLocation();
  const [value, setValue] = React.useState(null);

  const formik = useFormik({
    initialValues: initialSchema,
    validationSchema: ValidateSchema,
    onSubmit: (values) => {
      console.log('values', values);
    },
  });
  const options = {
    value,
    setValue,
  };

  useEffect(() => {
    if (state) {
      // set data in fields when updating
    }
  }, [state]);

  const handleUploadImage = (val) => {
    console.log('image val', val);
    const data = new FormData();
    data.append('photo', val);
    notifications
      .imageUpload(val)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.log(err));
    console.log('image', formik.values.photoId);
  };
  const submitData = () => {
    const data = {
      ...formik.values,
    };
    console.log('values', data);
    if (state) {
      // update
    } else {
      notifications
        .create(data)
        .then((resp) => {
          console.log('create response', resp);
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <>
      <Box
        sx={{
          p: 5,
          width: '80%',
          borderRadius: '8px',
        }}
      >
        <Grid container spacing={5}>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              User Type
            </Typography>
          </Grid>
          <Grid item md={6}>
            <SelectInput
              fullWidth
              size="small"
              name="userStatus"
              options={userType}
              helperText={formik.touched.userStatus && formik.errors.userStatus}
              error={Boolean(
                formik.touched.userStatus && formik.errors.userStatus
              )}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.userStatus}
            />
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Select a Country
            </Typography>
          </Grid>
          <Grid item md={6}>
            <SelectInput
              fullWidth
              size="small"
              name="countryId"
              options={Countries}
              helperText={formik.touched.countryId && formik.errors.countryId}
              error={Boolean(
                formik.touched.countryId && formik.errors.countryId
              )}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.countryId}
            />
          </Grid>
          <Grid item md={2}>
            <Typography my={1} variant="p" color={colors.grey[700]}>
              Total Users: 15,700
            </Typography>
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Select a Language
            </Typography>
          </Grid>
          <Grid item md={6}>
            <SelectInput
              fullWidth
              size="small"
              options={Langs}
              name="language"
              helperText={formik.touched.language && formik.errors.language}
              error={Boolean(formik.touched.language && formik.errors.language)}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.language}
            />
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Title
            </Typography>
          </Grid>
          <Grid item md={8}>
            <TextField
              fullWidth
              type="text"
              margin="normal"
              name="title"
              helperText={formik.touched.title && formik.errors.title}
              error={Boolean(formik.touched.title && formik.errors.title)}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.title}
              variant="outlined"
              size="small"
              autoComplete="off"
              sx={{ borderRadius: '8px' }}
            />
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Message
            </Typography>
          </Grid>
          <Grid item md={8}>
            <TextField
              fullWidth
              multiline
              rows={4}
              margin="normal"
              name="text"
              helperText={formik.touched.text && formik.errors.text}
              error={Boolean(formik.touched.text && formik.errors.text)}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.text}
              type="text"
              variant="outlined"
              size="small"
              autoComplete="off"
              sx={{ borderRadius: '8px' }}
            />
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Thumbnail
            </Typography>
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(formik.touched.photoId && formik.errors.photoId)}
              fullWidth
              helperText={formik.touched.photoId && formik.errors.photoId}
              margin="normal"
              name="photoId"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.photoId}
              variant="outlined"
              size="small"
              autoComplete="off"
              sx={{ borderRadius: '8px' }}
            />
          </Grid>
          <Grid
            item
            md={2}
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Button
              variant="contained"
              size="large"
              component="label"
              sx={{
                backgroundColor: '#8A8D9D',
                textTransform: 'Capitalize',
              }}
            >
              Upload File
              <input
                type="file"
                hidden
                onChange={(e) => handleUploadImage(e.target.value)}
              />
            </Button>
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Schedule
            </Typography>
          </Grid>
          <Grid
            item
            md={6}
            sx={{
              display: 'flex',
              gap: 6,
            }}
          >
            <Radio
              // checked={selectedValue === 'b'}
              // onChange={handleChange}
              name="radio"
              inputProps={{ 'aria-label': 'B' }}
              sx={{
                '& .MuiSvgIcon-root': {
                  fontSize: 28,
                },
              }}
            />

            <BasicDatePicker
              name="sentDate"
              getValues={options}
              initVal={value}
              setValue={setValue}
              helperText={formik.touched.sentDate && formik.errors.sentDate}
              error={Boolean(formik.touched.sentDate && formik.errors.sentDate)}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              // value={formik.values.sentDate}
            />
          </Grid>
          <Grid
            item
            columnSpacing={4}
            md={4}
            sx={{
              display: 'flex',
              gap: 6,
            }}
          >
            <input type="text" style={{ display: 'none' }} />
          </Grid>

          <Grid
            item
            md={6}
            sx={{
              display: 'flex',
              gap: 6,
            }}
          >
            <Radio
              inputProps={{ 'aria-label': 'B' }}
              sx={{
                '& .MuiSvgIcon-root': {
                  fontSize: 28,
                },
              }}
            />
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Send immediately
            </Typography>
          </Grid>
          <Grid item md={4}>
            <Typography
              fullWidth
              my={1}
              variant="h4"
              align="left"
              color={colors.grey[700]}
            >
              Test
            </Typography>
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(
                formik.touched.testPhoneNumber && formik.errors.testPhoneNumber
              )}
              fullWidth
              helperText={
                formik.touched.testPhoneNumber && formik.errors.testPhoneNumber
              }
              margin="normal"
              name="testPhoneNumber"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.testPhoneNumber}
              variant="outlined"
              size="small"
              autoComplete="off"
              sx={{ borderRadius: '10px' }}
            />
          </Grid>
          <Grid
            item
            md={2}
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Button
              variant="contained"
              size="large"
              sx={{
                backgroundColor: '#8A8D9D',
                textTransform: 'Capitalize',
              }}
            >
              Send a test
            </Button>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Box
                sx={{
                  py: 4,
                  width: '100%',
                  display: 'flex',
                  gap: 2,
                  justifyContent: 'flex-end',
                }}
              >
                <SaveBtn type="submit" onClick={submitData}>
                  {state ? 'Update' : 'Send'}
                </SaveBtn>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default CreateForm;
