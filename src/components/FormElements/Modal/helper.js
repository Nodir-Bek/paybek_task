import React from 'react';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';

const Block = [
  {
    name: 'A',
    value: 1,
  },
];
const Row = [
  {
    name: '1',
    value: 1,
  },
  {
    name: '2',
    value: 2,
  },
  {
    name: '3',
    value: 3,
  },
  {
    name: '4',
    value: 4,
  },
  {
    name: '5',
    value: 5,
  },
];
const Billing = [
  {
    name: '1',
    value: 1,
  },
  {
    name: '2',
    value: 2,
  },
  {
    name: '3',
    value: 3,
  },
  {
    name: '4',
    value: 4,
  },
  {
    name: '5',
    value: 5,
  },
];
const Supplier = [
  {
    name: 'Supplier1',
    value: 1,
  },
  {
    name: 'Supplier2',
    value: 2,
  },
  {
    name: 'Supplier3',
    value: 3,
  },
];

export const ProductInventory = ({ value, setValue }) => (
  <Box sx={{ p: 3 }}>
    <Grid container spacing={2}>
      <Grid item sm={12} lg={4} md={4} xl={4}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-helper-label">Block</InputLabel>
          <Select
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            value={value}
            size="small"
            label="Age"
            onChange={(e) => setValue(e.target.value)}
          >
            {Block.map((item) => (
              <MenuItem key={item.value} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item sm={12} lg={4} md={4} xl={4}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-helper-label">Row</InputLabel>
          <Select
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            value={value}
            size="small"
            label="Age"
            onChange={(e) => setValue(e.target.value)}
          >
            {Row.map((item) => (
              <MenuItem key={item.value} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item sm={12} lg={4} md={4} xl={4}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-helper-label">Col</InputLabel>
          <Select
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            value={value}
            size="small"
            label="Age"
            onChange={(e) => setValue(e.target.value)}
          >
            {Billing.map((item) => (
              <MenuItem key={item.value} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
    <FormControl fullWidth sx={{ mt: 1 }}>
      <InputLabel id="demo-simple-select-helper-label">Supplier</InputLabel>
      <Select
        margin="dense"
        labelId="demo-simple-select-helper-label"
        id="demo-simple-select-helper"
        value={value}
        size="small"
        label="Supplier"
        onChange={(e) => setValue(e.target.value)}
      >
        {Supplier.map((item) => (
          <MenuItem key={item.value} value={item.value}>
            {item.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
    <TextField
      size="small"
      margin="dense"
      id="name"
      label="Quantity"
      fullWidth
      variant="outlined"
    />
    <TextField
      size="small"
      margin="dense"
      id="name"
      label="Original price"
      fullWidth
      variant="outlined"
    />
  </Box>
);

export const ComfirmationStatus = () => (
  <>
    <h1 className="text-center">
      Are you sure you want to change the status ?
    </h1>
  </>
);

export const ComfirmationDelete = () => (
  <>
    <h1 className="text-center">Are you sure you want to delete ?</h1>
  </>
);
