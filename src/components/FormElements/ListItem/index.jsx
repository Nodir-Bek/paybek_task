import React, { useState } from 'react';
import { MenuItem } from '@mui/material';

const Item = ({ element, colorCompositionProps }) => {
  const [listData, setListData] = useState([]);
  const {
    colorSchema,
    compositionSchema,
    setColor,
    setComposition,
    brandsDchema,
  } = colorCompositionProps;

  if (element.name === 'color_id') {
    setListData((prev) => colorSchema);
  }
  if (element.name === 'commposition_id') {
    setListData((prev) => compositionSchema);
  }
  if (element.name === 'brand_id') {
    setListData((prev) => brandsDchema);
  }
  console.log('list item', listData);
  return (
    listData &&
    listData.map((listItem, index) => (
      <MenuItem key={`${index + 1}`} value={listItem.value}>
        {listItem.name}
      </MenuItem>
    ))
  );
};

export default Item;
