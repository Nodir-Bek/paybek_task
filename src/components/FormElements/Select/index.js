import React from 'react';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';

const Input = ({
  onChange, options = [], value, placeholder, status, size, label, id, ...rest
}) => (
  <Select
    {...rest}
    id={id}
    size={size}
    placeholder={placeholder}
    // input={<OutlinedInput/>}
    label={label}
    value={value}
    disabled={status}
    onChange={(value) => onChange(value)}
  >
    {options.map((item, index) => (
      <MenuItem key={`${index + 1}`} value={item.value}>
        {item.name}
      </MenuItem>
    ))}
  </Select>
);
export default Input;
