import React from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import { IOSSwitch } from './style';

export default ({ status }) => (
  <div>
    <FormControlLabel
      control={<IOSSwitch sx={{ m: 2 }} value={status} checked={status} />}
    />
  </div>
);
