import { useTheme } from '@mui/styles';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Chip from '@mui/material/Chip';
import { getStyles, MenuProps } from './style';

export default ({
  tags,
  deleteChips,
  handleChangeChips,
  selectedTags,
  name,
}) => {
  const theme = useTheme();
  return (
    <div>
      <Select
        labelId="demo-multiple-chip-label"
        id="demo-multiple-chip"
        multiple
        fullWidth
        size="small"
        name={name}
        value={selectedTags}
        onChange={handleChangeChips}
        input={<OutlinedInput id="select-multiple-chip" />}
        renderValue={(selected) => (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
            {selected.map((value, idx) => (
              <Chip
                key={`${idx + 1}`}
                onDelete={() => deleteChips(value)}
                value={value}
                onMouseDown={(event) => {
                  event.stopPropagation();
                }}
                label={value}
              />
            ))}
          </Box>
        )}
        MenuProps={MenuProps}
      >
        {tags.map((item, index) => (
          <MenuItem
            key={`${index + 1}`}
            value={item.tag_name}
            style={getStyles(item, selectedTags, theme)}
          >
            {item.tag_name}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};
