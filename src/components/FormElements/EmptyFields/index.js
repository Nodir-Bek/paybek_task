import React from 'react';
import TextField from '@mui/material/TextField';
import { HighlightOff as DeleteIcon } from '@mui/icons-material';
import { useStyles } from './style';

export default ({ index, handleDeleteField, formik }) => {
  const classes = useStyles();
  return (
    <>
      {formik.values.items[index].additionalProps.map((field, idx) => (
        <div className={classes.root} key={`myid ${idx + 1}`}>
          <TextField
            label="Key name"
            name={`items.${index}.additionalProps.${idx}.input_key`}
            onChange={formik.handleChange}
            value={formik.values.items[index].additionalProps[idx].input_key}
            variant="outlined"
            size="small"
          />
          <TextField
            label="Value"
            name={`items.${index}.additionalProps.${idx}.input_value`}
            onChange={formik.handleChange}
            value={formik.values.items[index].additionalProps[idx].input_value}
            size="small"
            variant="outlined"
          />
          <div className={classes.delFiledWrapper}>
            <DeleteIcon
              className={classes.delFiledbtn}
              onClick={() => handleDeleteField(field.id, index)}
            />
          </div>
        </div>
      ))}
    </>
  );
};
