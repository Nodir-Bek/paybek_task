import { createStyles, makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: theme.spacing(3),
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      gap: '20px',
    },
    delFiledWrapper: {
      color: 'red',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'start',
      cursor: 'pointer',
    },
    delFiledbtn: {
      '&:hover': {
        color: '#ff000090',
      },
    },
  })
);
