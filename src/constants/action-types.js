import forms from './actions/form';
import notifications from './actions/notififcations';
import auth from './actions/auth';
import application from './actions/application';
import lang from './actions/lang';
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  ...forms,
  ...auth,
  ...notifications,
  ...application,
  ...lang,
};
